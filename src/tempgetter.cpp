#include  "inc/datagetter.hh"

float TempGetter::getData()
{
 std::fstream file(File::_filePath ,std::fstream::in);
 float ans;

 if(!file.is_open())
   {
    std::cout << "File called : " << File::_filePath.filename() << "could not be opened.\n";
    return -1.0f;
   }

  file >> ans;
  ans /= 1000;

  file.close();

  return ans;

}
