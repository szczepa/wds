#include "inc/mainwindow.hh"
#include "inc/datagetter.hh"

#include <QApplication>

#include <thread>
#include <chrono>
#include <iomanip>


void print(IDataGetter * getter)
{
  float val  = getter->getData();

  std::cout<<"[ \n";
  for(int i = 0; i < static_cast<int>(val); ++i)
    {
      if(val > 60)
        std::cout <<"\033[41;31m \033[0m";
      if(val < 60)
        std::cout <<"\033[42;32m \033[0m";
    }

  std::cout<<"] \n";
  std::cout << std::fixed <<" Temperatura procesora to :"  << std::setprecision(2) << val << '\n';
}

int main(int argc, char *argv[])
{
//  QApplication a(argc, argv);
//  MainWindow w;
//  w.show();

 TempGetter * getter = new TempGetter;
 IDataGetter * tempgetter = getter;

 TempGetter * getter1 = new TempGetter;
 IDataGetter * tempgetter1 = getter1;

 getter->setPath("/sys/class/thermal/thermal_zone0/temp");
 getter1->setPath("/sys/class/thermal/thermal_zone1/temp");
 while(true)
   {

     //system("clear");

     print(tempgetter);
     print(tempgetter1);
     std::this_thread::sleep_for(std::chrono::milliseconds(100));

     system("clear");

   }


 return 0;
}
