#ifndef DATAGETTER_HH
#define DATAGETTER_HH

#include <iostream>
#include <experimental/filesystem>
#include <chrono>
#include <queue>
#include <thread>
#include <fstream>

#include <QDebug>

using FS_path = std::experimental::filesystem::path;

class IDataGetter
{
public:
  virtual float getData() = 0;
};

class File
{
protected:
  FS_path _filePath;
public:
  void setPath(const FS_path & fp_)
  {
    _filePath = fp_;
  }
};

class TempGetter final : public IDataGetter , public File
{
public:
  float getData() override;
};

class RamUsageGetter final : public IDataGetter, public File
{
public:
  float getData() override;
};

class CpuUsageGetter final : public IDataGetter , public File
{
public:
  float getData() override;
};
#endif // DATAGETTER_HH
